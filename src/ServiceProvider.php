<?php
namespace KDA\Laravel\Layouts\Alpine\Marquee;
use KDA\Laravel\PackageServiceProvider;
use KDA\Laravel\Traits\HasCommands;
use KDA\Laravel\Layouts\Facades\LayoutManager;

//use Illuminate\Support\Facades\Blade;
class ServiceProvider extends PackageServiceProvider
{
    use HasCommands;
    protected $packageName ='laravel-layouts-alpine-marquee';
    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }
    public function register()
    {
        parent::register();
    }
    /**
     * called after the trait were registered
     */
    public function postRegister(){
    }
    //called after the trait were booted
    protected function bootSelf(){
     /*   LayoutManager::registerScripts([
            'marquee.js' => '/assets/js/marquee.js',
        ])
        ->packageDirectory(__DIR__.'/../')
        ->vendor($this->packageName)
        ->manifest(__DIR__.'/../mix-manifest.json');*/
      //  LayoutManager::registerVendor($this->packageName,__DIR__.'/../');
    }
}
