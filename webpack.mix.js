let mix = require("laravel-mix");

mix
.setPublicPath('./')
.js("resources/js/marquee.js", "assets/js")
.version();